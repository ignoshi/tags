package main

import (
	"log"
	"net/http"
	"os"
	"time"

	gh "github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gitlab.com/ignoshi/tags/tags"
)

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/", tags.ListTags).Methods("GET")
	r.HandleFunc("/", tags.CreateTag).Methods("POST")
	lr := gh.LoggingHandler(os.Stdout, r)

	srv := &http.Server{
		Handler:      lr,
		Addr:         ":8000",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Println("Server started, Listening on :8000")
	log.Fatal(srv.ListenAndServe())
}
