FROM golang:1.8
WORKDIR /go/src/gitlab.com/ignoshi/tags
COPY . /go/src/gitlab.com/ignoshi/tags
RUN go get
EXPOSE 8000
CMD ["go", "run", "main.go"]
